Feature: selenium testing automation using the herokuapp website

Scenario: testing the login functionality of any application
	Given I am on the login page of the HerokuApp
	When I click on the "Basic Auth" Link
	Then Auth page should open
	When I enter "valid" Username and password
	Then the Response should be "Success"
	
Scenario: identifying broken images
	Given I have successfully loged in to the HerokuApp
	When I click on "Broken Images" link
	And I navigate to the "Broken Images" page
	Then I should see "2" broken images in the page
	
Scenario: Capturing table, canvas and buttons from unstable dom
	Given I have successfully loged in to the HerokuApp
	When I click on "Challenging DOM" link
	And I navigate to the "Challenging DOM" page
	Then I should be able to capture the data of the table, canvas and links