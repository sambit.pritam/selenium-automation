$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/features/herokuApp.feature");
formatter.feature({
  "name": "selenium testing automation using the herokuapp website",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "testing the login functionality of any application",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "I am on the login page of the HerokuApp",
  "keyword": "Given "
});
formatter.match({
  "location": "HerokuApp.i_am_on_the_login_page_of_the_HerokuApp()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on the \"Basic Auth\" Link",
  "keyword": "When "
});
formatter.match({
  "location": "HerokuApp.i_click_on_the_Link(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Auth page should open",
  "keyword": "Then "
});
formatter.match({
  "location": "HerokuApp.auth_page_should_open()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I enter \"valid\" Username and password",
  "keyword": "When "
});
formatter.match({
  "location": "HerokuApp.i_enter_Username_and_password(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the Response should be \"Success\"",
  "keyword": "Then "
});
formatter.match({
  "location": "HerokuApp.the_Response_should_be(String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "identifying broken images",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "I have successfully loged in to the HerokuApp",
  "keyword": "Given "
});
formatter.match({
  "location": "HerokuApp.i_have_successfully_loged_in_to_the_HerokuApp()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on \"Broken Images\" link",
  "keyword": "When "
});
formatter.match({
  "location": "HerokuApp.i_click_on_link(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I navigate to the \"Broken Images\" page",
  "keyword": "And "
});
formatter.match({
  "location": "HerokuApp.i_navigate_to_the_page(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should see \"2\" broken images in the page",
  "keyword": "Then "
});
formatter.match({
  "location": "HerokuApp.i_should_see_broken_images_in_the_page(String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
});