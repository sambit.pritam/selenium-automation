package com.automation.selenium.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.Before;

public class ContextSteps {

	private static boolean initialized = false;
	
	private WebDriver driver;
	
	@Before
	public void setup() throws Exception {
		if(!initialized) {
			// initialize the driver
			System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
			driver = new ChromeDriver();
			initialized=true;
		}
	}
	
	public WebDriver getDriver() {
		return this.driver;
	}
}
