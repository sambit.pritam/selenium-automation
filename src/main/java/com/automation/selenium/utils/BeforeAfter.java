package com.automation.selenium.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class BeforeAfter {
	
	public static WebDriver getChromeDriver() throws Exception {
		WebDriver driver = null;
		
		try {
			System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
			driver = new ChromeDriver();						
		} catch(Exception e) {
			throw e;
		}
		
		return driver;
	}
	
}
