package com.automation.selenium.utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ResourceLoader {

	private Properties properties = null;
	
	public ResourceLoader () {
		InputStream inputStream = null;
		
		try {
			this.properties = new Properties();
			ClassLoader loader = Thread.currentThread().getContextClassLoader();
			inputStream = loader.getResourceAsStream("locators.properties");
			properties.load(inputStream);
		} catch (FileNotFoundException e) {
			e.printStackTrace();			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String getProperty(String key) {
		return this.properties.getProperty(key);
	}
}
