package com.automation.selenium.core;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.automation.selenium.custom.exceptions.HandleException;
import com.automation.selenium.utils.ResourceLoader;
import com.automation.selenium.validations.ValidateElement;

public class AutomationHelper {

	private ResourceLoader property = new ResourceLoader();

	public int getBrokenImages(WebDriver driver, String name) {
		int invalidImageCount = 0;
		String propertyName = name.replaceAll(" ", ".").toLowerCase() + ".img";
		try {
			List<WebElement> allImages = driver.findElements(By.xpath(property.getProperty(propertyName)));

			for (WebElement eachImg : allImages) {

				if (!ValidateElement.verifyActiveImage(eachImg)) {
					invalidImageCount++;
				}

			}
		} catch (HandleException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return invalidImageCount;
	}
}
