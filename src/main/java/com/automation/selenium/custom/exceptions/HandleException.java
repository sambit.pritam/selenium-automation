package com.automation.selenium.custom.exceptions;

import okhttp3.internal.http2.ErrorCode;

public class HandleException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final ErrorCode code;
	
	public HandleException(ErrorCode code) {
		super();
		this.code = code;
	}

	public HandleException(String message, Throwable cause, ErrorCode code) {
		super(message, cause);
		this.code = code;
	}

	public HandleException(String message, ErrorCode code) {
		super(message);
		this.code = code;
	}

	public HandleException(Throwable cause, ErrorCode code) {
		super(cause);
		this.code = code;
	}
	
	public ErrorCode getCode() {
		return this.code;
	}
}
