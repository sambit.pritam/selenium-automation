package com.automation.selenium.validations;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.openqa.selenium.WebElement;

import com.automation.selenium.custom.exceptions.HandleException;

import okhttp3.internal.http2.ErrorCode;

public class ValidateElement {

	public static boolean verifyActiveImage(WebElement image) throws HandleException {
		
		try {
			HttpClient client = HttpClientBuilder.create().build();
			HttpGet request = new HttpGet(image.getAttribute("src"));
			HttpResponse response= client.execute(request);
			
			if(response.getStatusLine().getStatusCode() == 200) {
				return true;
			}
		} catch (Exception e) {
			throw new HandleException(e.getStackTrace().toString(), ErrorCode.CONNECT_ERROR);
		}
		return false;
	}
	
	public static boolean validateIgnoreCase(String expected, String actual) {
		
		return expected.equalsIgnoreCase(actual);
	}
}
