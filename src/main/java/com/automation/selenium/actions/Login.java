package com.automation.selenium.actions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.automation.selenium.utils.ResourceLoader;

public class Login {

	private ResourceLoader property = new ResourceLoader();
	
	public boolean login(WebDriver driver, boolean validate)  {

		String authUrl = property.getProperty("auth.url");
		String mainUrl = property.getProperty("main.url");		
		if (validate) {
			driver.get(authUrl);
			return loginSuccessful(driver);
		}else {
			driver.get(mainUrl);
		}
			
		
		return true;
	}
	
	public boolean loginSuccessful(WebDriver driver) {
		String messageXpath = "/html/body/div[2]/div/div/p";
		driver.findElement(By.xpath(property.getProperty("basic.auth"))).click();
		return driver.findElement(By.xpath(messageXpath)).getText().contains("Congratulation");
	}
	
	
}
