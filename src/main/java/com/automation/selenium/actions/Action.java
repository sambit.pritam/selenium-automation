package com.automation.selenium.actions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.automation.selenium.utils.ResourceLoader;
import com.automation.selenium.validations.ValidateElement;

public class Action {

	private ResourceLoader property = new ResourceLoader();
	
	public void performClick(WebDriver driver, String name) {
		String propertyName = name.replaceAll(" ", ".").toLowerCase();
		try {
			driver.findElement(By.xpath(property.getProperty(propertyName))).click();
		} catch(Exception e) {
			System.out.println("Inside Action.performClick: element " + property.getProperty(propertyName) + " not found at page " + driver.getCurrentUrl());
		}
	}
	
	public boolean validateUrl(WebDriver driver, String name) {
		String propertyName = name.replaceAll(" ", ".").toLowerCase() + ".url";
		return ValidateElement.validateIgnoreCase(property.getProperty(propertyName), driver.getCurrentUrl());
	}
}
