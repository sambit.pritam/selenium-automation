package com.automation.selenium.test.runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		features="src/test/resources/features",
		glue="com.automation.selenium.step.definitions",
		tags= {"~@avoid"},
		plugin = { "pretty", "html:target/cucumber-reports" }
		)
public class CucumberRunner {
	
}
