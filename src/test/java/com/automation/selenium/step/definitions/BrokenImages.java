package com.automation.selenium.step.definitions;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.automation.selenium.actions.Action;
import com.automation.selenium.actions.Login;
import com.automation.selenium.core.AutomationHelper;
import com.automation.selenium.utils.ContextSteps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class BrokenImages {

	private Login login = new Login();
	private Action action = new Action();
	private ContextSteps contextSteps;
	private WebDriver driver;
	boolean prevStepFailed = false;
	AutomationHelper automationHelper = new AutomationHelper();
	String pageName;
	
	public BrokenImages(ContextSteps contextSteps) {
		this.contextSteps = contextSteps;
	}
	
//	@Given("I have successfully loged in to the HerokuApp")
//	public void i_have_successfully_loged_in_to_the_HerokuApp() {
////		driver = contextSteps.getDriver();
////		prevStepFailed = login.login(driver, false);
//	}
//
//	@When("I click on {string} link")
//	public void i_click_on_link(String string) {
//		action.performClick(driver, string);
//	}
//
//	@When("I navigate to the {string} page")
//	public void i_navigate_to_the_page(String string) {
//		prevStepFailed = action.validateUrl(driver, string);
//		pageName = string;
//	}
//
//	@Then("I should see {string} broken images in the page")
//	public void i_should_see_broken_images_in_the_page(String string) {
//		Assert.assertEquals(Integer.valueOf(automationHelper.getBrokenImages(driver, pageName)), Integer.valueOf(string));
//	}
//	
}
