package com.automation.selenium.step.definitions;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.automation.selenium.actions.Action;
import com.automation.selenium.actions.Login;
import com.automation.selenium.core.AutomationHelper;
import com.automation.selenium.utils.BeforeAfter;
import com.automation.selenium.utils.ContextSteps;

import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class HerokuApp {
	
	private static WebDriver driver = null;
	private Login login = new Login();
	private boolean result = false;
	private Action action = new Action();
	private ContextSteps contextSteps;
	boolean prevStepFailed = false;
	AutomationHelper automationHelper = new AutomationHelper();
	String pageName;

	@Test(priority=0)
	@Given("I am on the login page of the HerokuApp")
	public void i_am_on_the_login_page_of_the_HerokuApp() throws Exception {
		driver = BeforeAfter.getChromeDriver();
	}

	@Test(priority=1)
	@When("I click on the {string} Link")
	public void i_click_on_the_Link(String string) {
		result = login.login(driver, true);
	}

	@Test(priority=2)
	@Then("Auth page should open")
	public void auth_page_should_open() {
		System.out.println("Login " + result);
	}

	@Test(priority=3)
	@When("I enter {string} Username and password")
	public void i_enter_Username_and_password(String string) {
		System.out.println("username used: admin & password used: admin");
	}

	@Test(priority=4)
	@Then("the Response should be {string}")
	public void the_Response_should_be(String string) {
		System.out.println("The response is " + string);
	}
	
	@Given("I have successfully loged in to the HerokuApp")
	public void i_have_successfully_loged_in_to_the_HerokuApp() {
//		driver = contextSteps.getDriver();
		prevStepFailed = login.login(driver, false);
	}

	@When("I click on {string} link")
	public void i_click_on_link(String string) {
		action.performClick(driver, string);
	}

	@When("I navigate to the {string} page")
	public void i_navigate_to_the_page(String string) {
		prevStepFailed = action.validateUrl(driver, string);
		pageName = string;
	}

	@Then("I should see {string} broken images in the page")
	public void i_should_see_broken_images_in_the_page(String string) {
		Assert.assertEquals(Integer.valueOf(automationHelper.getBrokenImages(driver, pageName)), Integer.valueOf(string));
	}
	
	
	@After
	public void tearDown() {
		driver.quit();
//		login.login(driver, false);
	}
}
